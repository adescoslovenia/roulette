package si.atei.domain;

public abstract class Bet {

    private final int money;

    protected Bet(int money) {
        super();
        this.money = money;
    }

    public int getMoney() {
        return money;
    }

    /**
     * Process the bet with given number on the wheel and calculate winnings (if any)
     *
     * @param number Number on the whell
     * @return
     */
    public final int playRound(int number) {
        if (won(number)) {
            return odds() * money;
        } else {
            return 0;
        }
    }

    /**
     * Odds to calculate winnings. Return a 1 to X ratio.
     *
     * @return factory for bet multiplication when a bet wins
     */
    protected abstract int odds();

    /**
     * Did the bet win for given number
     *
     * @param number number on the wheel
     * @return true if win
     */
    protected abstract boolean won(int number);

    public String toString() {
        return Integer.toString(money);
    }
}
