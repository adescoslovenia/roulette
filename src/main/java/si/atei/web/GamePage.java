package si.atei.web;

import java.util.Iterator;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import si.atei.domain.Game;
import si.atei.domain.Player;

public class GamePage extends WebPage {
    private static final long serialVersionUID = 1L;

    public GamePage(final PageParameters parameters) {
        add(new Label("version", getApplication().getFrameworkSettings().getVersion()));
        add(new RefreshingView<Player>("players") {
            @Override
            protected Iterator<IModel<Player>> getItemModels() {
                return new DefaultModelIterator(getGame().getPlayers());
            }

            @Override
            protected void populateItem(Item<Player> item) {
                item.add(new Label("name", new PropertyModel<String>(item.getModel(), "name")));
                item.add(new Label("money", new PropertyModel<Integer>(item.getModel(), "money")));
            }
        });
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        Game game = new Game();
        game.addPlayer("Pepe Morales", 1000);
        game.addPlayer("Texas Bob", 2000);
        game.addPlayer("Lola", 1000);
        setDefaultModel(Model.of(game));
    }

    public Game getGame() {
        return (Game) getDefaultModelObject();
    }
}
